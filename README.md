# NIF List Generator

Small script to generate valid portuguese NIF codes.

To check the usage one can just run the program, as follows.

```bash
./nif-list-generator.py
```
```
Formatting, taken from wikipedia:
É constituído por nove dígitos, sendo os oito primeiros sequenciais e o último um dígito de controlo.
O NIF pode pertencer a uma de várias gamas de números, definidas pelos dígitos iniciais, com as seguintes interpretações:
1 a 3: Pessoa singular, a gama 3 começou a ser atribuída em junho de 2019;
45: Pessoa singular. Os algarismos iniciais '45' correspondem aos cidadãos não residentes que apenas obtenham em território português rendimentos sujeitos a retenção na fonte a título definitivo;
5: Pessoa colectiva obrigada a registo no Registo Nacional de Pessoas Colectivas;
6: Organismo da Administração Pública Central, Regional ou Local;
70, 74 e 75: Herança Indivisa, em que o autor da sucessão não era empresário individual, ou Herança Indivisa em que o cônjuge sobrevivo tem rendimentos comerciais;
71: Não residentes colectivos sujeitos a retenção na fonte a título definitivo;
72: Fundos de investimento;
77: Atribuição Oficiosa de NIF de sujeito passivo (entidades que não requerem NIF junto do RNPC);
78: Atribuição oficiosa a não residentes abrangidos pelo processo VAT REFUND;
79: Regime excepcional - Expo 98;
8: 'empresário em nome individual' (actualmente obsoleto, já não é utilizado nem é válido);
90 e 91: Condomínios, Sociedade Irregulares, Heranças Indivisas cujo autor da sucessão era empresário individual;
98: Não residentes sem estabelecimento estável;
99: Sociedades civis sem personalidade jurídica.
O nono e último dígito é o dígito de controlo. É calculado utilizando o algoritmo módulo 11.)

Usage:
./nif-list-generator.py --startingdigits=<starting digits> --outfile=<output file>
./nif-list-generator.py --startingdigits=2 # is what you want in most cases.
./nif-list-generator.py --startingdigits=1,2,3,45,6,70,71,72,74,75,77,78,79,8,90,91,98,99 # for all possibilities.
```
