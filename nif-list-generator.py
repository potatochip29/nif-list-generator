#!/bin/python3
import sys


def check_digit(string_num):
    """
    Calculates the control digit (the last digit), according to the NIF formatting.
    You can read about it here: https://pt.wikipedia.org/wiki/N%C3%BAmero_de_identifica%C3%A7%C3%A3o_fiscal
    :param string_num: The first 8 digits, as a string
    :return: The corresponding control digit, as a string
    """
    if not string_num.isdigit():
        raise ValueError
    soma = sum([int(digit) * (9 - pos) for pos, digit in enumerate(string_num)])
    resto = soma % 11
    if resto == 0:
        return '0'
    if resto == 1:
        return '0'
    return str(11 - resto)


def usage():
    """
    Prints usage instructions to stdin.
    :return: None
    """
    print("Formatting, taken from wikipedia:")
    print("É constituído por nove dígitos, sendo os oito primeiros sequenciais e o último um dígito de controlo.")
    print("O NIF pode pertencer a uma de várias gamas de números, definidas pelos dígitos iniciais, com as seguintes interpretações:")
    print("1 a 3: Pessoa singular, a gama 3 começou a ser atribuída em junho de 2019;")
    print("45: Pessoa singular. Os algarismos iniciais '45' correspondem aos cidadãos não residentes que apenas obtenham em território português rendimentos sujeitos a retenção na fonte a título definitivo;")
    print("5: Pessoa colectiva obrigada a registo no Registo Nacional de Pessoas Colectivas;")
    print("6: Organismo da Administração Pública Central, Regional ou Local;")
    print("70, 74 e 75: Herança Indivisa, em que o autor da sucessão não era empresário individual, ou Herança Indivisa em que o cônjuge sobrevivo tem rendimentos comerciais;")
    print("71: Não residentes colectivos sujeitos a retenção na fonte a título definitivo;")
    print("72: Fundos de investimento;")
    print("77: Atribuição Oficiosa de NIF de sujeito passivo (entidades que não requerem NIF junto do RNPC);")
    print("78: Atribuição oficiosa a não residentes abrangidos pelo processo VAT REFUND;")
    print("79: Regime excepcional - Expo 98;")
    print("8: 'empresário em nome individual' (actualmente obsoleto, já não é utilizado nem é válido);")
    print("90 e 91: Condomínios, Sociedade Irregulares, Heranças Indivisas cujo autor da sucessão era empresário individual;")
    print("98: Não residentes sem estabelecimento estável;")
    print("99: Sociedades civis sem personalidade jurídica.")
    print("O nono e último dígito é o dígito de controlo. É calculado utilizando o algoritmo módulo 11.)")
    print()
    print("Usage:")
    print("./nif-list-generator.py --startingdigits=<starting digits> --outfile=<output file>")
    print("./nif-list-generator.py --startingdigits=2 # is what you want in most cases.")
    print("./nif-list-generator.py --startingdigits=1,2,3,45,6,70,71,72,74,75,77,78,79,8,90,91,98,99 # for all possibilities.")


if __name__ == '__main__':
    if len(sys.argv) <= 1:
        usage()
        exit()

    for arg in sys.argv:

        if arg.startswith("--outfile="):
            outfile = arg.split("=")[1]
            sys.stdout = open(outfile, "w")

        if arg.startswith("--startingdigits="):
            starting_digits_list = arg.split("=")[1].split(",")

    for starting_digits in starting_digits_list:

        # number of intermediate digits, those that are not at the start and are not a control digit at the end
        n_remaining_digits = 8 - len(starting_digits)

        # for every possible sequence of the first 8 digits, calculate the control digit
        for i in range(0, 10 ** n_remaining_digits):
            main_digits = starting_digits + str(i).zfill(n_remaining_digits)
            last_digit = check_digit(main_digits)
            nif = main_digits + last_digit
            print(nif)






